from PIL import Image


class ImageProjection:

    def get_pixel_data(self, image, x, y):
        return image.getpixel((x, y))
    
    def get_images_projection(self, path, target, num_images, projection_size):
        data = []
        target_list = []
        for cont in range(1, num_images + 1):
            image = Image.open(f"{path}-{cont}.jpeg")
            binary_mat = [[0] * image.width for _ in range(image.height)]
            
            for l in range(len(binary_mat)):
                for c in range(len(binary_mat[0])):
                    mean = sum(self.get_pixel_data(image, c, l)[:3]) // 3
                    binary_mat[l][c] = 0 if mean > 167 else 1
            
            projection_aux = [0] * len(binary_mat[0])
            
            for c in range(len(binary_mat[0])):
                for l in range(len(binary_mat)):
                    projection_aux[c] += binary_mat[l][c]
            
            projection = []
            move = len(projection_aux) // projection_size
            offset = 0
            
            for _ in range(projection_size):
                proj = sum(projection_aux[offset:offset+move])
                proj = proj // move  # Média
                projection.append(proj)
                offset += move
    
            data.append(",".join(str(proj) for proj in projection))
            target_list.append(target)
        return data, target_list


