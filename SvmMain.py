from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from ImageProjection import ImageProjection

def main():
    kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    C_values = [0.1, 1.0, 10.0]

    results_file = open("svm_results.txt", "w")

    for kernel in kernels:
        for C in C_values:
            svm = SVC(kernel=kernel, C=C)

            image_projection = ImageProjection()

            data_home, target_home = image_projection.get_images_projection('./home/images', 'it_is_a_home', 20, 50)
            data_not_home, target_not_home = image_projection.get_images_projection('./not_home/images', 'it_is_not_a_home', 20, 50)

            data_final = data_home + data_not_home
            target_final = target_home + target_not_home

            data_final = [list(map(float, projection.split(','))) for projection in data_final]

            X_train, X_test, y_train, y_test = train_test_split(data_final, target_final, test_size=0.5, random_state=42)
            
            svm.fit(X_train, y_train)
            y_pred = svm.predict(X_test)
            accuracy = accuracy_score(y_test, y_pred)

            results_file.write(f"Kernel: {kernel}, C: {C}, Taxa de sucesso: {accuracy}\n")
        
        results_file.write('-'*20)
        results_file.write('\n')

    results_file.close()


if __name__ == '__main__':
    main()