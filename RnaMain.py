from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from ImageProjection import ImageProjection
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix, classification_report

def main():
    results_file = open("rna_results.txt", "w")
    image_projection = ImageProjection()
    data_home, target_home = image_projection.get_images_projection('./home/images', 'it_is_a_home', 20, 50)
    data_not_home, target_not_home = image_projection.get_images_projection('./not_home/images', 'it_is_not_a_home', 20, 50)
    data_final = data_home + data_not_home
    target_final = target_home + target_not_home
    data_final = [list(map(float, projection.split(','))) for projection in data_final]
    X_train, X_test, y_train, y_test = train_test_split(data_final, target_final, test_size=0.4, random_state=42)

    mpl  = MLPClassifier(activation='identity', max_iter=50)
    mpl.fit(X_train, y_train)
    y_pred = mpl.predict(X_test)
    results_file.write(classification_report(y_test, y_pred))
    results_file.close()


if __name__ == '__main__':
    main()